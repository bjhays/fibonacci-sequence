<?php
function fibRecursive ($n)
{
	if ($n == 1 || $n == 2){return 1;}
	else{
		return fibonacci( $n - 1)+fibonacci( $n - 2 );
	}
}

function fibonacci($n) {
	//0, 1, 1, 2, 3, 5, 8, 13, 21, etc...

	if($n <0){return -1;}
	else if ($n == 0){return 0;}
	else if($n == 1 || $n == 2){return 1;}

	$int1 = 1;
	$int2 = 1;
	$fib = 0;

	for($i=1; $i<=$n-2; $i++ )
	{
		$fib = $int1 + $int2;
		$int2 = $int1;
		$int1 = $fib;
	}

	return $fib;
}


$res=array("X","Y","Z");
$rand=array();
echo "Results:".PHP_EOL;

for ($i=0; $i < 3 ; $i++) { 
	$rand[$i]=rand(1,100);
}

echo "Non Recusive:".PHP_EOL;
foreach ($rand as $i=>$fibNum) {
	$time_pre = microtime(true);
	$fib=fibonacci($fibNum);
	echo ' - '.$res[$i].', the '.$fibNum.'th fib, is '.$fib." - took ".(microtime(true) - $time_pre)." Sec".PHP_EOL;
}

echo "Recusive:".PHP_EOL;
foreach ($rand as $i=>$fibNum) {
	$time_pre = microtime(true);
	$fib=fibRecursive($fibNum);
	echo ' - '.$res[$i].', the '.$fibNum.'th fib, is '.$fib." - took ".(microtime(true) - $time_pre)." Sec".PHP_EOL;
}

die("Done".PHP_EOL);
?>